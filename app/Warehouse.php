<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

    //--Relations--//
    public function user() {
        return $this->belongsTo('App\User', 'managerId');
    }

	public function warehouseStocks() {
		return $this->hasMany('App\WarehouseStock', 'warehouseId', 'id');
	}

    public function shopRestockRequests() {
        return $this->hasMany('App\ShopRestockRequest', 'warehouseId', 'id');
    }
}
