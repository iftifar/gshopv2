<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopStock extends Model
{
    //--Relations--//
    public function shop() {
        return $this->belongsTo('App\Shop', 'shopId');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'productId');
    }
}
