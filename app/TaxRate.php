<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model
{
	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }
}
