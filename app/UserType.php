<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Validator;

class UserType extends Model
{
     use Notifiable;
     
     // For all variables to be mass assignable
	

	protected $fillable = [
		'name', 'remark'
	];

	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }
	
  	//--Relations--//
	public function users() {
		return $this->hasMany('App\User', 'typeId', 'id');
	}


	public function validationRules()
	{
		return [
            'name' => 'required|regex:/^[a-zA-Z]/'
        ];
	}

	public function validationMessages()
	{
		return [
            'name.required' => 'Name is Required',
            'name.regex' => 'First Character should be alpha'
        ];
	}

	public function validator($data, $rules, $messages)
	{
		return Validator::make($data, $rules, $messages);
	}
}
