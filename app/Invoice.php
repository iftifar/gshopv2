<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //--Relations--//
    public function user() {
        return $this->belongsTo('App\User', 'createdBy');
    }

    public function invoiceDetails() {
        return $this->hasMany('App\InvoiceDetail', 'invoiceId', 'id');
    }
}
