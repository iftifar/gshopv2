<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //--Mutetors--//
    public function setTitleAttribute($value) {
        $this->attributes['title'] = ucfirst($value);
    }

    public function setFirstNameAttribute($value) {
        $this->attributes['firstName'] = ucfirst($value);
    }

    public function setLastNameAttribute($value) {
        $this->attributes['lastName'] = ucfirst($value);
    }

    public function products() {
        return $this->belongsToMany('App\Product', 'product_supplier', 'supplierId', 'productId');
    }
}
