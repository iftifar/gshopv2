<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

	//--Relations--//
	public function products() {
		return $this->hasMany('App\Product', 'categoryId', 'id');
	}
}
