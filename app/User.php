<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'typeId','username', 'email', 'password','title','firstName','lastName','dob','sex','phoneNumber',
            'streetAddress','city','state','country','postcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    //--Mutetors--//
    public function setTitleAttribute($value) {
        $this->attributes['title'] = ucfirst($value);
    }

    public function setFirstNameAttribute($value) {
        $this->attributes['firstName'] = ucfirst($value);
    }

    public function setLastNameAttribute($value) {
        $this->attributes['lastName'] = ucfirst($value);
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    //--Relations--//
    public function userType() {
        return $this->belongsTo('App\UserType', 'typeId');
    }

    public function warehouses() {
        return $this->hasMany('App\Warehouse', 'managerId', 'id');
    }

    public function shops() {
        return $this->hasMany('App\Shop', 'managerId', 'id');
    }

    public function invoices() {
        return $this->hasMany('App\Invoice', 'createdBy', 'id');
    }

    public function shopRestockRequests() {
        return $this->hasMany('App\ShopRestockRequest', 'requestedBy', 'id');
    }
}
