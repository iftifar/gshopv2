<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
        'typeId'=>'required', 
        'username'=>'required|max:255',
        'email'=>'required|email|max:255|unique:users',
        'password'=>'required|min:6',
        'remember_token'=>'required',
        'title'=>'required',
        'firstName'=>'required',
        'lastName'=>'required',
        'dob'=>'required',
        'sex'=>'required',
        'phoneNumber'=>'required',
        'streetAddress'=>'required',
        'city'=>'required',
        'state'=>'required',
        'country'=>'required',
        'postcode'=>'required'
        
        ]);
    }

        

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
        'typeId'=>$data['typeId'], 
        'username'=>$data['username'],
        'email'=>$data['email'],
        'password'=>$data['password'],
        'remember_token'=>$data['remember_token'],
        'title'=>$data['title'],
        'firstName'=>$data['firstName'],
        'lastName'=>$data['lastName'],
        'dob'=>$data['dob'],
        'sex'=>$data['sex'],
        'phoneNumber'=>$data['phoneNumber'],
        'streetAddress'=>$data['streetAddress'],
        'city'=>$data['city'],
        'state'=>$data['state'],
        'country'=>$data['country'],
        'postcode'=>$data['postcode']
        ]);
    }
}
