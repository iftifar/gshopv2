<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

    //--Relations--//
    public function brand() {
        return $this->belongsTo('App\Brand', 'brandId');
    }

    public function category() {
        return $this->belongsTo('App\Category', 'categoryId');
    }

    public function warehouseStocks() {
        return $this->hasMany('App\WarehouseStock', 'productId', 'id');
    }

    public function shopStocks() {
        return $this->hasMany('App\ShopStock', 'productId', 'id');
    }

    public function invoiceDetails() {
        return $this->hasMany('App\InvoiceDetail', 'productId', 'id');
    }

    public function shopRestockRequestDetails() {
        return $this->hasMany('App\ShopRestockRequestDetail', 'shopRestockRequestId', 'id');
    }

    public function suppliers() {
        return $this->belongsToMany('App\Supplier', 'product_supplier', 'productId', 'supplierId');
    }
}
