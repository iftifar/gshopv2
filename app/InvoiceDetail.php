<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    //--Relations--//
    public function invoice() {
        return $this->belongsTo('App\Invoice', 'invoiceId');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'productId');
    }
}
