<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');*/

/*
//Resource Routs
Route::resource('photos', 'PhotoController');
METHOD    	LINK                    CONTROLLER  ROUTE NAME
GET 		/photos 				index 		photos.index
GET 		/photos/create 			create 		photos.create
POST 		/photos 				store 		photos.store
GET 		/photos/{photo} 		show 		photos.show
GET 		/photos/{photo}/edit 	edit 		photos.edit
PUT/PATCH 	/photos/{photo} 		update 		photos.update
DELETE 		/photos/{photo} 		destroy 	photos.destroy
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::resource('userTypes', 'UserTypeController');

Route::get('/home', 'HomeController@index');