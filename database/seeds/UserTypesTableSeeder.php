<?php

use Illuminate\Database\Seeder;

use App\UserType;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i < 3; $i++){
            $userType = new UserType([
                'name' => str_random(random_int(5, 10)),
                'remark' => str_random(random_int(0, 20))
            ]);
            $userType->save();
        }
    }
}
