<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		for($i=0; $i < 5; $i++){
			$category = new Category([
				'name' => str_random(10),
				'remark' => str_random(10)
			]);
			$category->save();
		}
    }
}
