<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\WarehouseStock;
use App\Warehouse;
use App\Product;

class WarehouseStocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 30; $i++){
			$warehouseStock = new WarehouseStock([
				'quantity' => $faker->randomFloat(2, 10, 100)
			]);

			$warehouse = Warehouse::find(random_int(1, 2));
			$product = Product::find(random_int(1, 30));

			$warehouseStock->warehouse()->associate($warehouse);
			$warehouseStock->product()->associate($product);

			// using try catch because of the 2 column unique key constrain.
			try{
				$warehouseStock->save();
			} catch(exception $e){
				$i--;
			}
		}
    }
}
