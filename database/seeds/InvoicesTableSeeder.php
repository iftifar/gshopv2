<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\User;
use App\Invoice;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
		for($i=0; $i < 5; $i++){
			$invoice = new Invoice([
				'totalAmount' => $faker->randomFloat(2, 10, 100),
				'discount' => $faker->randomFloat(2, 10, 100),
				'grossTotal' => $faker->randomFloat(2, 10, 100),
				'receievedAmount' => $faker->randomFloat(2, 10, 100),
				'returnAmount' => $faker->randomFloat(2, 10, 100)
			]);
			$user = User::find(random_int(1, 10));

			$invoice->user()->associate($user);

			$invoice->save();
		}
    }
}
