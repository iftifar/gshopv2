<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Brand;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
		for($i=0; $i < 5; $i++){
			$brand = new Brand([
				'name' => $faker->company,
				'remark' => str_random(10)
			]);
			$brand->save();
		}
    }
}
