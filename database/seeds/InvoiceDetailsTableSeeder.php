<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\InvoiceDetail;
use App\Invoice;
use App\Product;

class InvoiceDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 30; $i++){
			$invoiceDetail = new InvoiceDetail([
				'quantity' => $faker->randomFloat(2, 10, 100)
			]);

			$invoice = Invoice::find(random_int(1, 5));
			$product = Product::find(random_int(1, 30));

			$invoiceDetail->invoice()->associate($invoice);
			$invoiceDetail->product()->associate($product);

			// using try catch because of the 2 column unique key constrain.
			try{
				$invoiceDetail->save();
			} catch(exception $e){
				$i--;
			}
		}
    }
}
