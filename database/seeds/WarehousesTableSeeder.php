<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\User;
use App\Warehouse;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
		for($i=0; $i < 2; $i++){
			$warehouse = new Warehouse([
				'name' => $faker->unique()->name,
				'streetAddress' => $faker->streetAddress,
				'city' => $faker->city,
				'state' => 'Dhaka',
				'country' => $faker->country,
				'postcode' => $faker->postcode
			]);
			$user = User::find(random_int(1, 10));

			$warehouse->user()->associate($user);

			$warehouse->save();
		}
    }
}
