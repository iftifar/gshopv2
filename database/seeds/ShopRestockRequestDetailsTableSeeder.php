<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\ShopRestockRequestDetail;
use App\ShopRestockRequest;
use App\Product;

class ShopRestockRequestDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 10; $i++){
			$shopRestockRequestDetail = new ShopRestockRequestDetail([
				'quantity' => $faker->randomFloat(2,10,100)
			]);
			
			$shopRestockRequest = ShopRestockRequest::find(random_int(1, 5));
			$product = Product::find(random_int(1, 30));

			$shopRestockRequestDetail->shopRestockRequest()->associate($shopRestockRequest);
			$shopRestockRequestDetail->product()->associate($product);

			$shopRestockRequestDetail->save();
		}
    }
}
