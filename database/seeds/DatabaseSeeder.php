<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	//	factory(App\User::class, 10)->create();

		/**
		 * DO NOT CHANGE THE ORDER OF THE SEEDER CALLS
		 */

		$this->call(UserTypesTableSeeder::class);
		$this->call(SuppliersTableSeeder::class);
		$this->call(BrandsTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);

		$this->call(UsersTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
		
		$this->call(WarehousesTableSeeder::class);
		$this->call(ShopsTableSeeder::class);
		$this->call(InvoicesTableSeeder::class);
		$this->call(ProductSupplierTableSeeder::class);

		$this->call(WarehouseStocksTableSeeder::class);
		$this->call(ShopStocksTableSeeder::class);
		$this->call(ShopRestockRequestsTableSeeder::class);
		$this->call(InvoiceDetailsTableSeeder::class);

		$this->call(ShopRestockRequestDetailsTableSeeder::class);
	}
}
