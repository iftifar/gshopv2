<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Shop;
use App\Warehouse;
use App\User;
use App\ShopRestockRequest;

class ShopRestockRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 5; $i++){
			$shopRestockRequest = new ShopRestockRequest([
				'status' => $faker->word,
				'remark' => $faker->sentence
			]);
			
			$user = User::find(random_int(1, 10));
			$shop = Shop::find(random_int(1, 2));
			$warehouse = Warehouse::find(random_int(1, 2));

			$shopRestockRequest->user()->associate($user);
			$shopRestockRequest->shop()->associate($shop);
			$shopRestockRequest->warehouse()->associate($warehouse);

			$shopRestockRequest->save();
		}
    }
}
