<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('barCodeString');
            $table->string('barCodeImageUrl');
            $table->string('sku')->unique();
          
          //-- For Version Mismatch Problem

            if ((DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')) {
            $table->json('attributes')->nullable();
            } else {
                $table->text('attributes')->nullable();
            }
            
          
            $table->integer('brandId');
            $table->integer('categoryId');
            $table->float('unitPrice');
            $table->float('mrp');
            $table->string('batchNo');
            $table->date('manufactureDate');
            $table->date('expiryDate');
            $table->string('salesCodeString')->nullable();
            $table->string('salesCodeImageUrl')->nullable();
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
