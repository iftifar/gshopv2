@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						User Type List
						<a href="{{ url('/userTypes/create') }}"><button class="btn btn-primary">Add New User Type</button></a>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4"><strong>Name</strong></div>
							<div class="col-md-6"><strong>Remark</strong></div>
							<div class="col-md-2"><strong>Actions</strong></div>
						</div>
						@foreach($userTypes as $userType)
							<div class="row">
								<div class="col-md-4">
									<p>{{ $userType->name }}</p>
								</div>
								<div class="col-md-6">
									<p>{{ $userType->remark }}</p>
								</div>
								<div class="col-md-2">
									<a href="{{ url('/userTypes/'.$userType->id) }}"><button class="btn btn-sm btn-primary">View</button></a>
								</div>
							</div>
						@endforeach
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
