@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						User Type Detail
						<a href="{{ url('/userTypes/'.$userType->id.'/edit') }}"><button class="btn btn-primary">Edit</button></a>
						<form method="POST" action="{{ url('/userTypes/'.$userType->id) }}">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary">Delete</button>
						</form>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4"><strong>Name:</strong></div>
							<div class="col-md-8">{{ $userType->name }}</div>
						</div>
						<div class="row">
							<div class="col-md-4"><strong>Remark:</strong></div>
							<div class="col-md-8">{{ $userType->remark }}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
